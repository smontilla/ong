CREATE TABLE voluntario(
    id int(10) NOT NULL AUTO_INCREMENT, 
    nombre varchar(20),
    apellidos varchar(50),
    direccion varchar(100),
    telefono varchar(9),
    implicacion int(2),
    lenguajes varchar(20),
    PRIMARY KEY (`id`)
);

CREATE TABLE disponibilidad(
    id int(10) AUTO_INCREMENT, 
    dia int(4),
    hora int(4),
    PRIMARY KEY (`id`)
);

CREATE TABLE voluntarioDisponibilidad(
    id int(10) AUTO_INCREMENT, 
    idVoluntario varchar(9),
    idDisponibilidad int(4),
    PRIMARY KEY (`id`)
);

