<?php
class bd{
    const HOST = 'localhost';
    const BD = 'ong';
    const USUARIO = 'ong';
    const PASSWORD = 'ong_20102018';

    private $link;
    private $tabla;
    
    public function __construct($tabla) {
        $this->bd = 'ong';
        $this->tabla = $tabla;
        $this->link = new PDO('mysql:host=localhost;dbname=ong', self::USUARIO, self::PASSWORD);
    }

    private function getCampos( $datos ){
        $keys = array_keys($datos);             
        return implode(",", $keys );    
    }
    private function getKeysValuesCampos( $datos ){
        $keyCamposInsertar = array_keys( $datos );
        return ":". implode(", :", $keyCamposInsertar);     
    }
    private function getFieldEdit($datos){
        $valoresInsertar = array();
        foreach( $datos as $campo => $valor ){
            $valoresInsertar[ ] = $campo."=:".$campo;
        }
        return implode(',', $valoresInsertar);
    }
    private function getCondictions($datos){
        if( empty($datos) )return " 1";

        $valoresInsertar = array();
        foreach( $datos as $campo => $valor ){
            $valoresInsertar[ ] = $campo."=:".$campo;
        }
        return implode(' AND ', $valoresInsertar);
    }
    private function getValues($datos ){
        $valoresInsertar = array();
        foreach( $datos as $campo => $valor ){
            $valoresInsertar[ ':'.$campo ] = $valor;
        }
        return $valoresInsertar;
    }

    public function crear( $datos ){
        $resultadoOperacion = false;
        if( !empty($datos) ){
            $sql = "INSERT INTO ". $this->tabla ." (". $this->getCampos($datos) .") VALUES (". $this->getKeysValuesCampos($datos) .")";
            $consulta = $this->link->prepare( $sql );
            $resultadoConsulta = $consulta->execute( $this->getValues($datos) );
            if( $resultadoConsulta ){
                $resultadoOperacion = $this->link->lastInsertId();
            }
        }
        return $resultadoOperacion;
    }

    public function editar($id, $datos){
        $resultadoOperacion = false;
        if( !empty($datos) ){
            $valores = $this->getValues($datos);
            $valores[':id'] = $id;
            
            $sql = "UPDATE ". $this->tabla ." SET ". $this->getFieldEdit($datos) ." WHERE id=:id";
            $consulta = $this->link->prepare( $sql );
            $resultadoConsulta = $consulta->execute( $valores );
        }
        return $resultadoOperacion;
    }

    public function buscar($datos=array()){
        $sql = "SELECT * FROM ". $this->tabla ." WHERE ". $this->getCondictions($datos);
        $consulta = $this->link->prepare( $sql );
        $consulta->execute( $datos );
        $resultadoOperacion = $consulta->fetchAll( PDO::FETCH_ASSOC );
        
        return $resultadoOperacion;
    }

    public function borrarPorId($id){
        $consulta = $this->link->prepare( "DELETE FROM ". $this->tabla ." WHERE id=:id" );
        $resultadoOperacion = $consulta->execute( array(
            ':id' => $id
        ) );
        return $resultadoOperacion;
    }
}

$bd = new bd('voluntario');

/*
echo "usuario: ". $db->crear(array(
    "nombre" => "Santiago",
    "apellidos" => "Montilla",
    "direccion" => "Granada",
    "telefono" => "66666666666",
    "implicacion" => 3
));
*/
/*
echo $db->borrarPorId(104);
*/

// print_r( $bd->buscar() );


// echo $bd->editar(102, array("nombre" => "Santiago (editado)", "apellidos"=>"bbb") );