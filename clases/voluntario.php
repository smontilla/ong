<?php
include "nivelImplicacion.php";
include "franjaHoraria.php";
include "semana.php";
include "disponibilidad.php";
include "bd.class.php";

class Voluntario{

    // Connection instance
    private $connection;

    // table name
    private $table_name = "Voluntario";

    // table columns
    public $id;
    private $nombre;
    private $apellidos;
    private $direccion;
    private $telefono;
    private $implicacion;
    private $disponibilidad;
    private $lenguajes;
    

    public function __construct($connection){
        $this->connection = $connection;
    }
    
    public function crear($datos){
       $this->connection->crear($datos); 
   }

    
    public function editar($datos){
       $this->connection->editar($datos); 
    }
    
    public function obtener(){
       return $this->connection->buscar(array('id'=>$this->id)); 
    }
    
    public function eliminar($datos){
       $this->connection->eliminar($datos); 
    }
}

$voluntario = new Voluntario(new bd('voluntario'));


?>
