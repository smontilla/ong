<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../../clases/bd.class.php';
include_once '../../clases/voluntario.php';
 
$database = new Database();
$db = $database->getConnection();

$body = json_decode(file_get_contents('php://input'));
 
$voluntario = new Voluntario($db);
$voluntario->id = isset($body['id']) ? $body['id'] : die();
$voluntario->nombre = isset($body['nombre']) ? $body['nombre'] : die();
$voluntario->apellidos = isset($body['apellidos']) ? $body['apellidos'] : die();
$voluntario->direccion = isset($body['direccion']) ? $body['direccion'] : die();
$voluntario->telefono = isset($body['telefono']) ? $body['telefono'] : die();
$voluntario->implicacion = isset($body['implicacion']) ? $body['implicacion'] : die();
//lenguajes
//disponibilidad

if($voluntario->editar()){
    http_response_code(200);

    echo json_encode(
        array("message" => "Voluntario editado")
    );
}else{
    http_response_code(404);
 
    echo json_encode(
        array("message" => "No se ha podido editar.")
    );
}
?>
