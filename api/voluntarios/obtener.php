<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../../clases/voluntario.php';


$voluntario = new Voluntario( new bd('voluntario') );
$voluntario->id = isset($_GET['id']) ? $_GET['id'] : die();

$voluntarios = $voluntario->obtener();
$num = count($voluntarios);

if($num>0){
    http_response_code(200);

    echo json_encode($voluntarios);
}else{
    http_response_code(404);
 
    echo json_encode(
        array("message" => "No hay voluntarios.")
    );
}
?>