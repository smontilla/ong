<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
include_once '../../clases/bd.class.php';
include_once '../../clases/voluntario.php';
 
$database = new Database();
$db = $database->getConnection();
 
$voluntario = new Voluntario($db);
$voluntario->id = isset($_GET['id']) ? $_GET['id'] : die();

if($voluntario->eliminar()){
    http_response_code(200);

    echo json_encode(
        array("message" => "Voluntario eliminado")
    );
}else{
    http_response_code(404);
 
    echo json_encode(
        array("message" => "No se ha podido eliminar.")
    );
}
?>
